import java.util.ArrayList;
import java.util.List;

public abstract class Sujet {
    protected List<Observateur> observateurs;

    public Sujet() {
        observateurs = new ArrayList<Observateur>();
    }

    public void enregistreObservateur(Observateur electeur) {
        observateurs.add(electeur);
    }

    protected void notifieObservateurs(
            String attributModifie, Object nouvelleValeur) {
        for(Observateur o: observateurs){
           o.metsAJour(attributModifie, nouvelleValeur);
        }
            
    }
}
