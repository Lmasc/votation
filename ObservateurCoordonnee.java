
public class ObservateurCoordonnee implements Observateur {
    private String nom;
    private String modification = "PAS DE MODIFICATION";
    
    public ObservateurCoordonnee(String nom){
        this.nom =nom;
    }

    @Override
    public boolean metsAJour(String attributModifie,
    Object valeur) {
        modification = "Attribut : " + attributModifie
        + " Valeur : " + valeur;
    }

    public String getModification() {
        return modification;
    }
    
    public String getNom(){
        return this.nom;
    }

}
