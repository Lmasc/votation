
public interface Observateur {

    boolean metsAJour(String attributModifie,
    Object nouvelleValeur);

    String getModification();
}
